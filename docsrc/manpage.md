# conlog {#man-conlog}

Console logger for AQMS applications

## PAGE LAST UPDATED ON

2020-04-07

## NAME

conlog

## VERSION and STATUS

v 2016-06-03  
status: ACTIVE  

## PURPOSE

Log console output of aqms applications to a log file.  

## HOW TO RUN

```
Program: conlog 2016-06-03
Arguments:
Required argument not found.

usage: conlog [flags] <log file>

where flags can be:
	-h            -- this help
	-l            -- enable creation and updates to symlink file %s_current.log
	-L <symlink>  -- enable creation and updates to specified symlink file
```

## CONFIGURATION FILE

None

## ENVIRONMENT

None

## DEPENDENCIES

* Conlog depends on the the aqms-libs: tnstd, tntime

## MAINTENANCE

None

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-conlog/-/issues

## MORE INFORMATION

