How to compile and install conlog {#how-to-compile-install-conlog}
==================================================
# Prerequisites

Conlog depends on aqms-libs. Ensure that these are compiled and available.  

# Compile  

Run  

make -f Makefile  

This will generate a binary named *conlog*.  

# Install  

Copy *conlog* to desired location. 





