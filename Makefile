########################################################################
#
# Makefile     : conlog Program
#
# Author       : Patrick Small
#
# Last Revised : September 2, 1999
#
########################################################################


########################################################################
# Standard definitions and macros
#
########################################################################

include $(DEVROOT)/shared/makefiles/Make.includes
BIN	= conlog

INCL	= $(RTSTDINCL)

LIBS	= -L$(RTLIBDIR) -ltnstd -ltntime -ltnstd $(QLIB2LIBS) $(SVR4LIBS)
#LIBS	= ../lib/tntime/libtntime.a ../lib/tnstd/libtnstd.a $(QLIB2LIBS) $(SVR4LIBS)
#LIBS	= $(RTLIBDIR)/libtntime.a $(RTLIBDIR)/libtnstd.a $(QLIB2LIBS) $(SVR4LIBS)

OBJS = ConsoleLog.o

# builder info
HOSTNAME=$(shell hostname | cut -d'.' -f1)
BUILDER=$(USER)@$(HOSTNAME)
CFLAGS+= -DBUILDER=\"$(BUILDER)\"

all:${BIN}

conlog:${OBJS}
	${CC} $(CFLAGS) ${OBJS} -o $@ ${LIBS}

.C.o: 
	$(CC) $< -c $(CFLAGS) $(INCL) 

clean:
	-rm -f *.o *~ core ${BIN} 
	-rm  -rf SunWS_cache

